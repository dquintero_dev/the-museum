namespace The_Museum.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CATEGORIAS
    {
        [Key]
        [StringLength(4)]
        public string ca_codigo { get; set; }

        [StringLength(50)]
        public string ca_nombre { get; set; }
    }
}
