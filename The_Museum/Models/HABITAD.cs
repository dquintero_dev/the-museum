namespace The_Museum.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HABITAD")]
    public partial class HABITAD
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ha_codigo { get; set; }

        [StringLength(50)]
        public string ha_nombre { get; set; }
    }
}
