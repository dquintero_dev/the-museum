namespace The_Museum.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ANIMALES
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int an_codigo { get; set; }

        public int? an_cod_re { get; set; }

        public int? an_cod_cl { get; set; }

        public int? an_cod_ha { get; set; }

        [StringLength(100)]
        public string an_especie { get; set; }

        [StringLength(100)]
        public string an_nombre { get; set; }

        public string an_caracteristicas { get; set; }

        public string an_comportamiento { get; set; }

        [StringLength(4)]
        public string an_cod_ca { get; set; }

        public string an_img1 { get; set; }

        public string an_img2 { get; set; }
    }
}
