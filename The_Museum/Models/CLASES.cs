namespace The_Museum.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CLASES
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cl_codigo { get; set; }

        [Required]
        [StringLength(50)]
        public string cl_nombre { get; set; }
    }
}
