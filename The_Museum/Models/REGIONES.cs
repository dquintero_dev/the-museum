namespace The_Museum.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class REGIONES
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int re_codigo { get; set; }

        [Required]
        [StringLength(50)]
        public string re_nombre { get; set; }
    }
}
