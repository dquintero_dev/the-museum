namespace The_Museum.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model : DbContext
    {
        public Model()
            : base("name=Museum")
        {
        }

        public virtual DbSet<ANIMALES> ANIMALES { get; set; }
        public virtual DbSet<CATEGORIAS> CATEGORIAS { get; set; }
        public virtual DbSet<CLASES> CLASES { get; set; }
        public virtual DbSet<HABITAD> HABITAD { get; set; }
        public virtual DbSet<REGIONES> REGIONES { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ANIMALES>()
                .Property(e => e.an_especie)
                .IsUnicode(false);

            modelBuilder.Entity<ANIMALES>()
                .Property(e => e.an_nombre)
                .IsUnicode(false);

            modelBuilder.Entity<ANIMALES>()
                .Property(e => e.an_caracteristicas)
                .IsUnicode(false);

            modelBuilder.Entity<ANIMALES>()
                .Property(e => e.an_comportamiento)
                .IsUnicode(false);

            modelBuilder.Entity<ANIMALES>()
                .Property(e => e.an_cod_ca)
                .IsUnicode(false);

            modelBuilder.Entity<ANIMALES>()
                .Property(e => e.an_img1)
                .IsUnicode(false);

            modelBuilder.Entity<ANIMALES>()
                .Property(e => e.an_img2)
                .IsUnicode(false);

            modelBuilder.Entity<CATEGORIAS>()
                .Property(e => e.ca_codigo)
                .IsUnicode(false);

            modelBuilder.Entity<CATEGORIAS>()
                .Property(e => e.ca_nombre)
                .IsUnicode(false);

            modelBuilder.Entity<CLASES>()
                .Property(e => e.cl_nombre)
                .IsUnicode(false);

            modelBuilder.Entity<HABITAD>()
                .Property(e => e.ha_nombre)
                .IsUnicode(false);

            modelBuilder.Entity<REGIONES>()
                .Property(e => e.re_nombre)
                .IsUnicode(false);
        }
    }
}
