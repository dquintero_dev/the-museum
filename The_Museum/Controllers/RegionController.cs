﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using The_Museum.Models;

namespace The_Museum.Controllers
{
    public class RegionController : Controller
    {
        Model db = new Model();
        // GET: Region
        public ActionResult Index(int id)
        {
            ViewBag.Title = db.REGIONES.Where(x => x.re_codigo == id).Select(y => y.re_nombre).FirstOrDefault().ToUpper();
            return View(db.ANIMALES.Where(x => x.an_cod_re == id).ToList());
        }

        public ActionResult Animals(int id)
        {
            ANIMALES ani = db.ANIMALES.Where(x => x.an_codigo == id).FirstOrDefault();
            ViewBag.cl = db.CLASES.Where(x => x.cl_codigo == ani.an_cod_cl).Select(y => y.cl_nombre).FirstOrDefault();
            ViewBag.ha= db.HABITAD.Where(x => x.ha_codigo == ani.an_cod_ha).Select(y => y.ha_nombre).FirstOrDefault();
            ViewBag.ca= db.CATEGORIAS.Where(x => x.ca_codigo == ani.an_cod_ca).Select(y => y.ca_nombre).FirstOrDefault();
            ViewBag.Title = ani.an_nombre.ToUpper();
            return View(db.ANIMALES.Where(x => x.an_codigo == id).FirstOrDefault());
        }
    }
}